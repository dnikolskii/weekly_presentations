#!/usr/bin/env python3
# -*- coding: utf-8 -*
# Created by dmitrii at 02.02.2025

import pandas as pd
from sklearn.tree import DecisionTreeRegressor, plot_tree
import matplotlib.pyplot as plt

data = {
    'days_of_life': [10, 12, 14, 17, 20],
    'tte_lag_1': [3, 2, 2, 3, 3],
    'tte': [2, 2, 3, 3, 5]
}

df = pd.DataFrame(data)

X = df[['days_of_life', 'tte_lag_1']]
y = df['tte']

model = DecisionTreeRegressor(max_depth=1)
model.fit(X, y)

plt.figure(figsize=(8, 4))
plot_tree(
    model,
    feature_names=['days_of_life'],
    filled=True,
    rounded=True,
    node_ids=True,
    fontsize=10
)
plt.savefig('dtree.png', dpi=300, bbox_inches='tight')
plt.close()

# new_data = pd.DataFrame({'days_of_life': [15, 19]})
# predictions = model.predict(new_data)
# print(f"{new_data=}, {predictions=}")

